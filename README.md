Teste no Wordpress com o Gatling
=================================

Olá, esse é um projeto que estou utilizando para coletar resultados relativos ao meu 
Trabalho de Conclusão de Curso.

O projeto basicamente consiste em executar um teste de estresse em um servidor hospedando
o Wordpress, mas pode ser facilmente adaptado, basta dar uma olhada na API do Gatling.

Sinta-se livre para olhar e modificar para as suas necessidades