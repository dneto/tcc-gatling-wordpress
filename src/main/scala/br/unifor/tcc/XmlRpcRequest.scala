package br.unifor.tcc

import scala.collection.mutable.MutableList
import scala.xml._


class XmlRpcRequest($methodCall: String, $parameters: MutableList[String]) {
  var methodCall = $methodCall
  var parameters: MutableList[String] = $parameters

  def parameter(parameter: String): XmlRpcRequest = {
    parameters += parameter
    return this
  }

  def toXml(): String = {
    val xmlStr =
      <methodCall>
        <methodName>
          {methodCall}
        </methodName>
        <params>
          {for (param <- parameters) yield
          <param>
            <value>
              {param}
            </value>
          </param>}
        </params>
      </methodCall>;
    return "<?xml version='1.0'?>" + xmlStr.toString()
  }
}

object XmlRpc {
  def xmlRpc(methodName: String): XmlRpcRequest = {
    val xmlReq = new XmlRpcRequest(methodName, new MutableList[String])
    return xmlReq
  }
}
