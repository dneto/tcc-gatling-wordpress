package br.unifor.tcc

import br.unifor.tcc.scenarios.{DeletePost, EditPost, NewPost}
import io.gatling.core.Predef._

class WordpressTest extends Simulation {

  val newPost = scenario("New Post").exec(NewPost.newPost)
  val editPost = scenario("Edit Post").exec(NewPost.newPost, EditPost.editPost)
  val deletePost = scenario("Delete Post").exec(NewPost.newPost, DeletePost.deletePost)

  setUp(
    newPost.inject(atOnceUsers(50)).protocols(Config.httpConf),
    editPost.inject(atOnceUsers(50)).protocols(Config.httpConf),
    deletePost.inject(atOnceUsers(50)).protocols(Config.httpConf)
  )

}
