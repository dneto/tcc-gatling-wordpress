package br.unifor.tcc

import io.gatling.http.Predef._
import io.gatling.core.Predef._


object Config {
  val ADDRESS = "54.207.28.140"
  val ADMIN = "crawler-admin"
  val PASSWORD = "crawler-admin"

  val httpConf = http
    .baseURL("http://"+Config.ADDRESS+"/wordpress/")
    .acceptHeader("*/*")
    .acceptEncodingHeader("gzip,deflate,sdch")
    .acceptLanguageHeader("en,pt-BR;q=0.8,pt;q=0.6,en-US;q=0.4")
    .userAgentHeader("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36")
}
