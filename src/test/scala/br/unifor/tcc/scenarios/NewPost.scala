package br.unifor.tcc.scenarios

import br.unifor.tcc.Config
import br.unifor.tcc.XmlRpc._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object NewPost {
  val newPost = exec(http("New Post")
    .post("/xmlrpc.php")
    .body(StringBody(
    xmlRpc("blogger.newPost")
      parameter ("not empty")
      parameter ("not empty")
      parameter (Config.ADMIN)
      parameter (Config.PASSWORD)
      parameter ("Novo Porta")
      parameter ("true")
      toXml())).check(regex("""<int>([^"]*)</int>""").saveAs("postId")))
}
