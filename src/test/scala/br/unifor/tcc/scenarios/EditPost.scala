package br.unifor.tcc.scenarios

import br.unifor.tcc.Config
import br.unifor.tcc.XmlRpc._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object EditPost {
  val editPost =
    exec(http("Edit Post")
    .post("/xmlrpc.php")
    .body(StringBody(
    xmlRpc("blogger.editPost")
      parameter ("not empty")
      parameter ("${postId}")
      parameter (Config.ADMIN)
      parameter (Config.PASSWORD)
      parameter ("Novo Editado")
      parameter ("true")
      toXml()))
    )
}
