package br.unifor.tcc.scenarios

import br.unifor.tcc.Config
import br.unifor.tcc.XmlRpc._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object DeletePost {
  val deletePost =
    exec(http("Delete Post")
      .post("/xmlrpc.php")
      .body(StringBody(
      xmlRpc("blogger.deletePost")
        parameter ("not empty")
        parameter ("${postId}")
        parameter (Config.ADMIN)
        parameter (Config.PASSWORD)
        parameter ("true")
        toXml()))
    )
}
