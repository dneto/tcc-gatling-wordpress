var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "250",
        "ok": "0",
        "ko": "250"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "60116",
        "ok": "-",
        "ko": "60116"
    },
    "meanResponseTime": {
        "total": "36030",
        "ok": "-",
        "ko": "36030"
    },
    "standardDeviation": {
        "total": "29418",
        "ok": "-",
        "ko": "29418"
    },
    "percentiles1": {
        "total": "60087",
        "ok": "-",
        "ko": "60087"
    },
    "percentiles2": {
        "total": "60107",
        "ok": "-",
        "ko": "60107"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 250,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "4,14",
        "ok": "-",
        "ko": "4,14"
    }
},
contents: {
"new-post-43eab": {
        type: "REQUEST",
        name: "New Post",
path: "New Post",
pathFormatted: "new-post-43eab",
stats: {
    "name": "New Post",
    "numberOfRequests": {
        "total": "150",
        "ok": "0",
        "ko": "150"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60116",
        "ok": "-",
        "ko": "60116"
    },
    "meanResponseTime": {
        "total": "60050",
        "ok": "-",
        "ko": "60050"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "-",
        "ko": "27"
    },
    "percentiles1": {
        "total": "60095",
        "ok": "-",
        "ko": "60095"
    },
    "percentiles2": {
        "total": "60112",
        "ok": "-",
        "ko": "60112"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 150,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "2,49",
        "ok": "-",
        "ko": "2,49"
    }
}
    },"delete-post-2e304": {
        type: "REQUEST",
        name: "Delete Post",
path: "Delete Post",
pathFormatted: "delete-post-2e304",
stats: {
    "name": "Delete Post",
    "numberOfRequests": {
        "total": "50",
        "ok": "0",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "meanResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles2": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 50,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0,83",
        "ok": "-",
        "ko": "0,83"
    }
}
    },"edit-post-17ebf": {
        type: "REQUEST",
        name: "Edit Post",
path: "Edit Post",
pathFormatted: "edit-post-17ebf",
stats: {
    "name": "Edit Post",
    "numberOfRequests": {
        "total": "50",
        "ok": "0",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "meanResponseTime": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles2": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "group1": {
        "name": "t < 800 ms",
        "count": 0,
        "percentage": 0
    },
    "group2": {
        "name": "800 ms < t < 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group3": {
        "name": "t > 1200 ms",
        "count": 0,
        "percentage": 0
    },
    "group4": {
        "name": "failed",
        "count": 50,
        "percentage": 100
    },
    "meanNumberOfRequestsPerSecond": {
        "total": "0,83",
        "ok": "-",
        "ko": "0,83"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
